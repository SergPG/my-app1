import React, {useState} from 'react';
import './App.css';
import Counter from'./components/Counter';

function App() {
    
   const [value, setValue] = useState('String ...');
   

    return (
    <div className="App">
      <header className="App-header">

      <Counter />
          
      <h3> {value} </h3>

      <input type="text"
             value={value}
             onChange = {event => setValue(event.target.value)}

        />
    
       
      </header>
    </div>
    );
}

export default App;